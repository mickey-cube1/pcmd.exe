/* *** This file is based on ReactOS: src/base/shell/cmd/precomp.h . *** */
#ifndef __CMD_PRECOMP_H
#define __CMD_PRECOMP_H

#ifdef _MSC_VER
#pragma warning ( disable : 4103 ) /* use #pragma pack to change alignment */
#undef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE
#endif /*_MSC_VER */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <malloc.h>
#include <tchar.h>

#define WIN32_NO_STATUS
#include <windef.h>
#include <winbase.h>
#include <winnls.h>
#include <winreg.h>
#include <winuser.h>
#include <wincon.h>
#include <shellapi.h>
#include <winternl.h>
#include <winioctl.h>

#define NTOS_MODE_USER
// [FIXME:20191205]
#define ASSERT( exp )         ((void) 0)
//#include <ndk/rtlfuncs.h>

#include <strsafe.h>

#include <conutils.h>

#include "resource.h"

#include "cmd.h"
#include "config.h"
#include "batch.h"

#include <wine/debug.h>
WINE_DEFAULT_DEBUG_CHANNEL(cmd);
#ifdef UNICODE
#define debugstr_aw debugstr_w
#else
#define debugstr_aw debugstr_a
#endif

#endif /* __CMD_PRECOMP_H */
